<?php
declare(strict_types=1);

namespace App\CounterTable\Services;

use App\CounterTable\Entities\AllocationEntity;
use App\CounterTable\Entities\CounterTableEntity;
use App\CounterTable\Entities\GroupEntity;
use App\Etc\Time;
use Exception;
use JsonException;
use function abs;
use function array_filter;
use function array_key_last;
use function array_values;

class CounterTableService {

    /**
     * @todo should be a entity collection
     * @var AllocationEntity
     */
    private array $allocations = [];

    /**
     * @throws JsonException
     */
    public function __construct(
        private readonly CounterTableEntity $counterTableEntity,
        private readonly Time $time,
        private readonly bool $isTest = false
    )
    {
        $this->allocations = $this->loadAllocations();
    }

    /**
     * @return AllocationEntity
     */
    public function getAllocations(): array
    {
        return $this->allocations;
    }

    /**
     * @throws Exception
     */
    public function place(GroupEntity $group): AllocationEntity
    {
        // make group immutable
        $group = clone $group;
        $this->updateAllocationsByTime();
        return $this->allocatePlace(
            $group,
            $this->findBestPlace(
                $group,
                $this->getPossiblePlaces($group)
            ),
        );
    }

    /**
     * @return list<int>
     * @throws Exception
     */
    public function getPossiblePlaces(GroupEntity $group): array {
        // the keys of $this->allocations where after are enough places
        $possiblePlacesAfterAllocationsKeys = [];

        if (empty($this->allocations)) {
            if (!$this->hasEnoughSeatsInGeneral($group)) {
                throw new Exception('There is not enough space!');
            }
        } else {
            $lastKey = array_key_last($this->allocations);
            foreach ($this->allocations as $key => $allocation) {
                // null if not exists
                $nextAllocationsKey = $this->getNextAllocationsKey($key);
                $nextAllocation = $nextAllocationsKey === null ? null : $this->allocations[$nextAllocationsKey];
                if ($this->hasEnoughSeatsInBetween($group, $allocation, $nextAllocation)) {
                    $possiblePlacesAfterAllocationsKeys[] = $key;
                }
            }
            if (empty($possiblePlacesAfterAllocationsKeys)) {
                throw new Exception('There is not enough space!');
            }
        }

        return $possiblePlacesAfterAllocationsKeys;
    }

    private function getNextAllocationsKey($currentKey) {
        if (count($this->allocations) <= 1) {
            return null;
        }
        if ($currentKey === array_key_last($this->allocations)) {
            return array_key_first($this->allocations);
        }
        return $currentKey + 1;
    }

    /**
     * @param array{
     *      'allocationsKey': int,
     *      'firstSeatPos': int,
     *      'lastSeatPos': int,
     *  } $place
     * @throws Exception
     * @todo a transaction should be used to ensure data integrity for parallel requests
     */
    public function allocatePlace(GroupEntity $group, array $place): AllocationEntity
    {
        if ($this->hasPlaces($group, $place)) {
            $newAllocation = new AllocationEntity(
                $group,
                $this->time->getTimeStamp() + ($group->getResidenceTimeInMin() * 60),
                $place['firstSeatPos'],
                $place['lastSeatPos']
            );

            array_splice(
                $this->allocations,
                $place['allocationsKey'],
                0,
                [$newAllocation]
            );

            $this->saveAllocations($this->allocations);


            return $newAllocation;
        }
        // todo: that is not enough, it could happen that there are still enough places but the complete array maybe changed in the meanwhile and the place is not that it was supposed to be
        throw new Exception('Allocations changed in the meanwhile, the free places are gone');
    }


    /**
     * @param array{
     *      'allocationsKey': int,
     *      'firstSeatPos': int,
     *      'lastSeatPos': int,
     *  } $place
     */
    public function hasPlaces(GroupEntity $group, array $place): bool
    {
        // TODO
        return true;
//        if (empty($this->allocations)) {
//            return true;
//        }
//        $allocationBefore = $place['allocationsKey'] === 0
//            ? $this->allocations[array_key_last($this->allocations)]
//            : $this->allocations[$place['allocationsKey'] - 1];
//        $first = $this->calculateFirstSeatPosition(
//            $allocationBefore
//        );
//        $last = $this->calculateLastSeatPosition(
//            $allocationBefore,
//            $group->getNumberOfPersons()
//        );
//
//        return $first !== $place['firstSeatPos'] || $last !== $place['lastSeatPos'];
    }

    /**
     * @param list<int> $possiblePlacesAfterAllocationsKeys
     * @return array{
     *      'allocationsKey': int,
     *      'firstSeatPos': int,
     *      'lastSeatPos': int,
     *  }
     */
    public function findBestPlace(GroupEntity $groupEntity, array $possiblePlacesAfterAllocationsKeys): array
    {
        if (empty($possiblePlacesAfterAllocationsKeys)) {
            return [
                'allocationsKey' => 0,
                'firstSeatPos' => 0,
                'lastSeatPos' => $groupEntity->getNumberOfPersons() - 1,
            ];
        }
        
        return [
            'allocationsKey' => $possiblePlacesAfterAllocationsKeys[0] + 1,
            'firstSeatPos' => $this->calculateFirstSeatPosition(
                $this->allocations[$possiblePlacesAfterAllocationsKeys[0]]->getLastSeatPos()
            ),
            'lastSeatPos' => $this->calculateLastSeatPosition(
                $this->allocations[$possiblePlacesAfterAllocationsKeys[0]]->getLastSeatPos(),
                $groupEntity->getNumberOfPersons()
            ),
        ];        
    }

    public function deallocatePlace(int $groupId): void
    {

    }

    private function hasEnoughSeatsInBetween(
        GroupEntity $group,
        AllocationEntity $recent,
        ?AllocationEntity $next
    ): bool
    {
        // there is only one group already allocated todo this is wrong
        if ($next === null) {
            return ($this->counterTableEntity->getSeats() - $recent->getGroup()->getNumberOfPersons()) > $group->getNumberOfPersons();
        }
        return ($recent->getLastSeatPos() + $group->getNumberOfPersons() < $next->getFirstSeatPos())
            ||
            (
                ($recent->getLastSeatPos() > $next->getFirstSeatPos())
                && (($this->counterTableEntity->getSeats() - $recent->getLastSeatPos() + $next->getFirstSeatPos()) >= $group->getNumberOfPersons())
            );
    }

    private function hasEnoughSeatsInGeneral(
        GroupEntity $group,
    ): bool
    {
        return $group->getNumberOfPersons() <= $this->counterTableEntity->getSeats();
    }


    /**
     * @param int $leftSeatPos
     * @return int
     */
    private function calculateFirstSeatPosition(int $leftSeatPos): int {
        return ($leftSeatPos < $this->counterTableEntity->getSeats() - 1) ? $leftSeatPos + 1 : 0;
    }

    private function calculateLastSeatPosition(int $leftSeatPos, int $numberOfPersons): int {
        $x = $leftSeatPos + $numberOfPersons;
        return ($x < $this->counterTableEntity->getSeats()) ? $x : abs($x - $this->counterTableEntity->getSeats());
    }

    /**
     * Removes groups that already left
     */
    private function updateAllocationsByTime(): void
    {
        if (!empty($this->allocations)) {
            $this->allocations = array_values(
                array_filter(
                    $this->allocations,
                    fn(AllocationEntity $allocationEntity) => $allocationEntity->getOccupiedUntil() >= $this->time->getTimeStamp()
                )
            );
        }
    }


    /**
     * @throws JsonException
     */
    private function saveAllocations(array $allocations): void
    {
        if ($this->isTest) {
            return;
        }
        file_put_contents(
            __DIR__ . '/../../../allocations.json',
            json_encode(
                array_map(
                    fn (AllocationEntity $allocationEntity) => $allocationEntity->toArray(),
                    $allocations
                ),
                JSON_THROW_ON_ERROR
            )
        );
    }

    /**
     * @throws JsonException
     */
    private function loadAllocations(): array {
        if ($this->isTest) {
            return [];
        }
        return array_map(
            fn ($map) => new AllocationEntity(
                new GroupEntity(
                    $map['group']['numberOfPersons'],
                    $map['group']['residenceTimeInMin'],
                ),
                $map['occupiedUntil'],
                $map['firstSeatPos'],
                $map['lastSeatPos']
            ),
            json_decode(
                file_get_contents(__DIR__ . '/../../../allocations.json'),
                true,
            ) ?? []
        );
    }
}