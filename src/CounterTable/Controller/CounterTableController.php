<?php

namespace App\CounterTable\Controller;

use App\CounterTable\Entities\AllocationEntity;
use App\CounterTable\Entities\CounterTableEntity;
use App\CounterTable\Entities\GroupEntity;
use App\CounterTable\Services\CounterTableService;
use App\Etc\Time;
use JsonException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CounterTableController extends AbstractController
{
    private CounterTableService $counterTableService;


    /**
     * just test wise
     * @throws JsonException
     */
    public function __construct()
    {
        $this->counterTableService = new CounterTableService(
            new CounterTableEntity(
                77
            ),
            new Time()
        );
    }

    /**
     * @throws JsonException
     */
    #[Route('/api/counter_table', name: 'api_counter_table_set', methods: ['POST'])]
    public function setCounterTable(Request $request): Response
    {
        $this->counterTableService = new CounterTableService(
            new CounterTableEntity(
                $request->get('seats')
            ),
            new Time()
        );
        return new Response();
    }

    #[Route('/api/allocations', name: 'api_allocations_get_all', methods: ['GET'])]
    public function getAllocations(): Response
    {
        // implementation
        return $this->json(
            array_map(
                fn (AllocationEntity $allocationEntity) => $allocationEntity->toArray(),
                $this->counterTableService->getAllocations()
            )
        );
    }

    #[Route('/api/allocations/{id<\d+>}', name: 'api_allocation_get_one', methods: ['GET'])]
    public function getAllocation(int $id): Response
    {
        // implementation
        return $this->json(null);
    }

    /**
     * @throws \Exception
     */
    #[Route('/api/allocations', name: 'api_allocations_set', methods: ['POST'])]
    public function allocatePlace(Request $request): Response
    {
        $this->counterTableService->allocatePlace(
            new GroupEntity(
                (int) $request->get('numberOfPersons'),
                (int) $request->get('residenceTimeInMin'),
            ),
            [
                'allocationsKey' => (int) $request->get('allocationsKey'),
                'firstSeatPos' => (int) $request->get('firstSeatPos'),
                'lastSeatPos' => (int) $request->get('firstSeatPos') + ($request->get('numberOfPersons') - 1),
            ]
        );
        return new Response();
    }

    #[Route('/api/group', name: 'api_group_create', methods: ['POST'])]
    public function setGroup(Request $request): Response
    {


        return $this->json(
            ['no' => $request->getContent()]
        );
    }


}
