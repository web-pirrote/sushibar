<?php
declare(strict_types=1);
namespace App\CounterTable\Entities;

class CounterTableEntity {
    public function __construct(private readonly int $seats)
    {
    }

    public function getSeats(): int
    {
        return $this->seats;
    }
}