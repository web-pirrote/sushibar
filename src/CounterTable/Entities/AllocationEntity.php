<?php
declare(strict_types=1);
namespace App\CounterTable\Entities;

class AllocationEntity {
    public function __construct(
        private readonly GroupEntity $group,
        private readonly int         $occupiedUntil,
        private readonly int         $firstSeatPos,
        private readonly int         $lastSeatPos,
    )
    {
    }

    public function getGroup(): GroupEntity
    {
        return $this->group;
    }

    public function getOccupiedUntil(): int
    {
        return $this->occupiedUntil;
    }

    public function getFirstSeatPos(): int
    {
        return $this->firstSeatPos;
    }

    public function getLastSeatPos(): int
    {
        return $this->lastSeatPos;
    }

    public function toArray(): array {
        return [
            'group' => $this->getGroup()->toArray(),
            'occupiedUntil' => $this->getOccupiedUntil(),
            'firstSeatPos' => $this->getFirstSeatPos(),
            'lastSeatPos' => $this->getLastSeatPos(),
        ];
    }
}