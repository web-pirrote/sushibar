<?php
declare(strict_types=1);
namespace App\CounterTable\Entities;

class GroupEntity {
    public function __construct(
        private int $numberOfPersons,
        private int $residenceTimeInMin,
    )
    {
    }

    /**
     * @return int int
     */
    public function getNumberOfPersons(): int
    {
        return $this->numberOfPersons;
    }

    /**
     * @return int int
     */
    public function getResidenceTimeInMin(): int
    {
        return $this->residenceTimeInMin;
    }

    public function toArray(): array {
        return [
            'numberOfPersons' => $this->getNumberOfPersons(),
            'residenceTimeInMin' => $this->getResidenceTimeInMin(),
        ];
    }
}