<?php
declare(strict_types=1);
namespace App\Etc;

class Time {
    public function __construct(
        private int $unixtime = 0
    )
    {
    }

    public function add(int $min): int
    {
        return $this->unixtime += $min * 60;
    }

    public function getTimeStamp(): int
    {
        return $this->unixtime;
    }

    public function reset(): void
    {
        $this->unixtime = 0;
    }
}