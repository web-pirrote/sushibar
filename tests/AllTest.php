<?php
declare(strict_types=1);

namespace App\Tests;


use App\CounterTable\Entities\AllocationEntity;
use App\CounterTable\Entities\CounterTableEntity;
use App\CounterTable\Entities\GroupEntity;
use App\CounterTable\Services\CounterTableService;
use App\Etc\Time;
use PHPUnit\Framework\TestCase;

class AllTest extends TestCase
{
    private CounterTableService $counterTableService;
    private Time $time;

    protected function setUp(): void
    {
        parent::setUp();

        $this->time = new Time();
        $this->counterTableService = new CounterTableService(
            new CounterTableEntity(1000),
            $this->time,
            true
        );
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        $this->time->reset();
    }

    public function testOneGroup(): void {
        $this->time->add(10);
        static::assertEquals(
            new AllocationEntity(
                $group = new GroupEntity(1000, 30),
                2400,
                0,
                999
            ),
            $this->counterTableService->place($group)
        );
    }

    public function testTooBigGroup(): void {
        $exception = null;

        try {
            $this->counterTableService->place(
                new GroupEntity(1001, 30)
            );
        } catch (\Exception $exception) {

        }

        static::assertInstanceOf(\Exception::class, $exception);
        static::assertEquals(
            'There is not enough space!',
            $exception->getMessage()
        );
    }

    public function testGroupSpanningOverEdge(): void {
        $this->time->add(10);
        static::assertEquals(
            new AllocationEntity(
                $group0 = new GroupEntity(100, 20),
                1800,
                0,
                99
            ),
            $this->counterTableService->place($group0)
        );

        $this->time->add(10);
        static::assertEquals(
            new AllocationEntity(
                $group1 = new GroupEntity(850, 30),
                3000,
                100,
                949
            ),
            $this->counterTableService->place($group1)
        );

        $this->time->add(11);
        static::assertEquals(
            new AllocationEntity(
                $group2 = new GroupEntity(88, 30),
                3660,
                950,
                37
            ),
            $this->counterTableService->place($group2)
        );
    }
}